#ifndef RESOURCE_PACK_H
#define RESOURCE_PACK_H

#include "core/reference.h"

#include "core/resource.h"
#include "core/io/file_access_pack.h"

class ResourcePack : public Reference {
    GDCLASS(ResourcePack, Reference)

    PackedData *data = nullptr;

    protected:
    static void _bind_methods();

    public:
    Error add_pack(const String &p_path, bool p_replace_files = true, int p_offset = 0);
    RES load(const String &p_path, const String &p_type_hint = "", bool p_no_cache = false);
    void expose(bool p_expose);

    ResourcePack();
    ~ResourcePack();
};

#endif
