#include "resource_pack.h"

#include "core/io/resource_loader.h"

void ResourcePack::_bind_methods() {
    ClassDB::bind_method(D_METHOD("add_pack", "path", "replace_files", "offset"), &ResourcePack::add_pack, DEFVAL(false), DEFVAL(0));
    ClassDB::bind_method(D_METHOD("load", "path", "type_hint", "no_cache"), &ResourcePack::load, DEFVAL(""), DEFVAL(false));
    ClassDB::bind_method(D_METHOD("expose", "expose"), &ResourcePack::expose);
}

Error ResourcePack::add_pack(const String &p_path, bool p_replace_files, int p_offset) {
    return data->add_pack(p_path, p_replace_files, p_offset);
}

RES ResourcePack::load(const String &p_path, const String &p_type_hint, bool p_no_cache) {
    ERR_FAIL_COND_V_MSG(PackedData::get_more_data() != nullptr, RES(), "A resource pack is already in use");
    PackedData::set_more_data(data);
    RES ret = ResourceLoader::load(p_path, p_type_hint, p_no_cache);
    PackedData::set_more_data(nullptr);
    return ret;
}

void ResourcePack::expose(bool p_expose) {
    PackedData::set_more_data(p_expose ? data : nullptr);
}

ResourcePack::ResourcePack() {
    data = memnew(PackedData);
}

ResourcePack::~ResourcePack() {
    memdelete(data);
}
